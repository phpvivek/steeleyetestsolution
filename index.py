import json
import urllib.request
from datetime import datetime

import boto3
import xlrd


def get_json_data():
    """
    Function extract data from the sheet
    and return a dict

    :return: object
    """
    try:
        link = 'https://www.iso20022.org/sites/default/files/ISO10383_MIC/ISO10383_MIC.xls'
        file_name, headers = urllib.request.urlretrieve(link)
        workbook = xlrd.open_workbook(file_name)
        worksheet = workbook.sheet_by_name("MICs List by CC")

        # storing first row
        header = []
        for col in range(worksheet.ncols):
            header.append(worksheet.cell_value(0, col))

        # storing workbook to a list
        data = []
        for row in range(1, worksheet.nrows):
            elm = {}
            for col in range(worksheet.ncols):
                elm[header[col]] = worksheet.cell_value(row, col)

            data.append(elm)
        # with open('data.json', 'w') as outfile:
        #     json.dump(data, outfile)
        return data
    except Exception as ex:
        print(str(ex))
        return None


def upload_to_s3():
    """
    Uploads the json to AWS S3 in the specified bucket
    and the return the status

    :return: object
    """
    try:
        data = get_json_data()
        if data is not None:
            AWS_BUCKET_NAME = 'my-bucket-name'
            s3 = boto3.resource('s3')
            bucket = s3.Bucket(AWS_BUCKET_NAME)
            path = datetime.now().strftime("%Y-%m-%d%H:%I:%S") + '.json'
            bucket.put_object(
                ACL='public-read',
                ContentType='application/json',
                Key=path,
                Body=data,
            )
            body = {
                "uploaded": "true",
                "bucket": AWS_BUCKET_NAME,
                "path": path
            }
            return {"statusCode": 200, "body": json.dumps(body)}
        else:
            return {"statusCode": 204, "body": "No data found"}
    except Exception as ex:
        print(ex)


def lambda_handler(event, context):
    """
    This function is invoked by AWS Lambda
    :param event:
    :param context:
    :return: string
    """
    print("event " + str(event))
    print("context " + str(context))
    response = upload_to_s3()
    print(response)
    return 'completed'
